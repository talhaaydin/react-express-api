import React, { Component } from 'react';
import axios from 'axios';

class PostCreate extends Component {

    constructor(props) {
        super(props);

        this.onChangePostTitle = this.onChangePostTitle.bind(this);
        this.onChangePostContent = this.onChangePostContent.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            userID : 2,
            title: '',
            content: '',            
        }        
    }

    onChangePostTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangePostContent(e) {
        this.setState({
            content: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        const newPost = {
            userID: this.state.userID,
            title: this.state.title,
            content: this.state.content
        }

        axios.post('api/post/create', newPost)
            .then(response => console.log(response.data))
            .catch(err => console.error(err));

            window.location = '/';
    }


    render() {
        return (
            <div>
                <h3 className="mb-2 text-center">Create New Post</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Title </label>
                        <input  
                            type="text"
                            className="form-control"
                            value={this.state.title}
                            onChange={this.onChangePostTitle}
                        />
                    </div>
                    <div className="form-group">
                        <label>Content </label>
                        <textarea 
                            className="form-control"  
                            value={this.state.content}
                            onChange={this.onChangePostContent}
                            rows="4"
                        />                        
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create Post" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}

export default PostCreate;