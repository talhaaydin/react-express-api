import React, { Component } from 'react';
import axios from 'axios';

class PostEdit extends Component {
    constructor(props) {
        super(props);

        this.onChangePostTitle = this.onChangePostTitle.bind(this);
        this.onChangePostContent = this.onChangePostContent.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            userID: 2,
            title: '',
            content: ''
        }
    }

    onChangePostTitle(e) {
        this.setState({
            title: e.target.value
        })
    }

    onChangePostContent(e) {
        this.setState({
            content: e.target.value
        })
    }

    componentDidMount(){
        axios.get(`/api/post/${this.props.match.params.id}`)
            .then(response => 
                this.setState({
                    userID: response.data.userID,
                    title: response.data.title,
                    content: response.data.content
                })
            )
            .catch(err => console.error(err));
    }

    onSubmit(e) {
        e.preventDefault();

        const updatePost = {
            userID: this.state.userID,
            title: this.state.title,
            content: this.state.content
        }

        axios.put(`/api/post/update/${this.props.match.params.id}`, updatePost)
            .then(response => console.log(response.data));

            window.location = '/';
    }

    render() {
        return (
            <div>
                <h3 className="mb-2 text-center">Edit Post</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Title </label>
                        <input  
                            type="text"
                            className="form-control"
                            value={this.state.title}
                            onChange={this.onChangePostTitle}
                        />
                    </div>
                    <div className="form-group">
                        <label>Content </label>
                        <textarea 
                            className="form-control"  
                            value={this.state.content}
                            onChange={this.onChangePostContent}
                            rows="4"
                        />                        
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Edit Post" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
} 

export default PostEdit;