import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

// Component imports
import PostList from './components/post-list';
import PostCreate from './components/post-create';
import PostEdit from './components/post-edit';
import Login from './components/login';
import Register from "./components/register";
import withAuth from './components/with-auth';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLogin: false
    }
  }

  componentDidMount(){
    if(localStorage.getItem('auth-token'))
      this.setState({ isLogin: true })
  }

  handleLogout = () => {
    localStorage.removeItem('auth-token');
    window.location = '/login';
  }

  render () {
    return (
      <Router>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
            <div className="container">
              <Link to="/" className="navbar-brand">ReactExpressBlogAPI</Link>
              <div className="collpase navbar-collapse">
                <ul className="navbar-nav ml-3 mr-auto">
                  { 
                    this.state.isLogin ? 
                    <Fragment>
                      <li className="navbar-item">
                        <Link to="/" className="nav-link">Posts</Link>
                      </li>
                      <li className="navbar-item">
                        <Link to="/create" className="nav-link">Create Post</Link>
                      </li>  
                    </Fragment>  : ''                  
                  } 
                </ul>
                <ul className="navbar-nav my-2">
                  { 
                    this.state.isLogin ? 
                    <li className="navbar-item">
                        <a className="nav-link" onClick={this.handleLogout} style={{cursor: 'pointer'}}>Logout</a>
                    </li> : 
                    <Fragment>
                      <li className="navbar-item">
                        <Link to="/login" className="nav-link">Login</Link>
                      </li>
                      <li className="navbar-item">
                        <Link to="/register" className="nav-link">Register</Link>
                      </li> 
                    </Fragment>                  
                  } 
                </ul>
              </div>
            </div>
          </nav>

          <div className="container">
            <Route path="/" exact component={withAuth(PostList)} />
            <Route path="/edit/:id" component={withAuth(PostEdit)} />
            <Route path="/create" component={withAuth(PostCreate)} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </div>          
      </Router>
    )
  }
}

export default App;
